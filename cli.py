#!/usr/bin/env python

from __future__ import print_function
import argparse
import requests
import sys
import base64
import json
import argparse

def main():

    arg_parser = argparse.ArgumentParser(
            description='Given a fixture in the form of a Datasift doc, emulate transmission of a batch')
    arg_parser.add_argument('script')
#    arg_parser.add_argument('input', type=argparse.FileType('r'), default=sys.stdin)
    (args, rem) = arg_parser.parse_known_args()
    print(rem)

    urlfmt = "http://127.0.0.1:5000/{script}?args={argblob}"
    url = urlfmt.format(script=args.script,
                        argblob=base64.encodestring(json.dumps(rem)))

    print(url)
    #r = requests.post(url, data=sys.stdin)
    r = requests.get(url)

    for line in r.iter_lines():
        print(line)


if __name__ == "__main__":
    main()
