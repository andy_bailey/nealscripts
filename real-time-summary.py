# -- coding: utf-8 --
from __future__ import division
import argparse
import requests
import csv, codecs, cStringIO

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

# verizon - Basic QzIxREYzNDctOTA3RS00NDNCLUI2NUEtQUEzQzI1OTIzOUFBOg==
# nfl - Basic NmFiNmMwMjEtNjBiNS00ZmM2LWE0ZGEtN2Q2NWEwMzRiODVjOg==
# airlines - Basic MzFlNjhhMTctYjlkMS00YzNhLTg4MTQtMzM4ODczMzJlMDk3Og==

def main(client_nodes, tgt_file, params, clv, accept_rate):
    auths = {'verizon': 'Basic QzIxREYzNDctOTA3RS00NDNCLUI2NUEtQUEzQzI1OTIzOUFBOg==',
             'nfl': 'Basic NmFiNmMwMjEtNjBiNS00ZmM2LWE0ZGEtN2Q2NWEwMzRiODVjOg==',
             'airlines': 'Basic MzFlNjhhMTctYjlkMS00YzNhLTg4MTQtMzM4ODczMzJlMDk3Og=='}
    with open(tgt_file, 'w') as f:
        csv_writer = UnicodeWriter(f)
        columns = ['datetime', 'date', 'time', 'username', 'sentiment', 'userUrl', 'sourceInfo.retweets', 'sourceInfo'
                                                                                                 '.likeCount',
                   'source', 'avatar', 'images', 'messageUrl', '_id', 'content']
        clv_columns = ["sentiment level", "clv multiplier", "social multiplier", "is_last", "last_level",
                       "content_type", "real-time CLV", "Social Amplification"]

        csv_writer.writerow([''])
        csv_writer.writerow([''] * 8 + ['CLV', str(clv)])
        csv_writer.writerow([''] * 8 + ['Accept Rate', str(accept_rate)])
        csv_writer.writerow([''])

        csv_writer.writerow(['client', 'topic', 'id'] + columns + clv_columns)
        row_number = 5
        node_start_row = 5
        for node in client_nodes:
            if node['id']:
                print node['client'], node['topic'], node['id']
                authorization = auths[node['client'].lower()]
                i = 0
                total = 1
                r_count = 0
                node_start_row = row_number + 1
                first = True
                while True:
                    i += 1
                    print node['client'], node['topic'], node['id'], i
                    headers = {'content-type': 'application/json', 'Authorization': authorization}
                    url_request = 'http://dataengine.predictivescience' + \
                    '.com/dp/v1/nodes/' + str(node['id']) + \
                    '/socialstream?'+params+'&page=' + str(i)

                    r = requests.get(url_request, headers=headers)
                    #write to csv
                    if r.status_code == 200:
                        for result in r.json().get('_items',{}):
                            r_count += 1
                            row_number += 1
                            row = [node['client'], node['topic'], node['id']]
                            for c in columns:
                                if c == 'datetime':
                                    dt = unicode(get_value(result, key=c))
                                if c == 'date':
                                    v = dt.split("T")[0]
                                elif c == 'time':
                                    v = dt.split("T")[1]
                                else:
                                    v = unicode(get_value(result, key=c))
                                row.append(v)

                            # sentiment level
                            row.append('=IF(H' + str(row_number) + '=0,"neutral",IF(H' + str(row_number) +
                                       '>=1,"positive","negative"))')
                            # clv multiplier
                            row.append('=IF($H' + str(row_number) + ' > 0, 1, IF($H' + str(row_number) + ' = 0, 0, -1))')
                            # social multiplier
                            row.append('=IF($H' + str(row_number) + ' > 0, 1, IF($H' + str(row_number) + ' = 0, 0, -1))')
                            # is_last
                            if first:
                                row.append('=IF(AND(COUNTIF(G$5:G5, '
                                           'G' + str(row_number) + ')=0, J' + str(row_number) + ' = 0), TRUE, FALSE)')
                            else:
                                row.append('=IF(AND(COUNTIF(G$' + str(node_start_row) + ':G' + str(row_number-1) + ', '
                                       'G' + str(row_number) + ')=0, J' + str(row_number) + ' = 0), TRUE, FALSE)')
                            # last_level
                            row.append('=IF(U' + str(row_number) + '=TRUE,R' + str(row_number) + ',"")')
                            # content-type
                            row.append('=IF(J6=0,"original","retweet")')
                            # real-time CLV
                            row.append('=IF(U' + str(row_number) + '=TRUE,$S' + str(row_number) + '*$J$2,0)')
                            # social amplification
                            row.append('=IF(W' + str(row_number) + '="retweet",$T' + str(row_number) + '*$J$2*$J$3,0)')

                            csv_writer.writerow(row)
                            first = False

                        total = r.json().get('_meta',{}).get('total') if r.json() else None
                        if not total or r_count >= total or i >= total:
                            break
                    else:
                        print "error:", r.status_code, r.url, r.text
                        break

def main_agg(client_nodes, tgt_file, params, clv, accept_rate):
    auths = {'verizon': 'Basic QzIxREYzNDctOTA3RS00NDNCLUI2NUEtQUEzQzI1OTIzOUFBOg==',
             'nfl': 'Basic NmFiNmMwMjEtNjBiNS00ZmM2LWE0ZGEtN2Q2NWEwMzRiODVjOg==',
             'airlines': 'Basic MzFlNjhhMTctYjlkMS00YzNhLTg4MTQtMzM4ODczMzJlMDk3Og=='}
    with open(tgt_file, 'w') as f:
        csv_writer = UnicodeWriter(f)
        input_cols = ['arpu', 'population', 'confidence_required', 'margin_of_error', 'discount_rate',
                   'standard_deviation']
        sample_cols = ['sample_required', 'total_users', 'assured_users', 'assured_percent',
                   'neutral_users', 'neutral_percent', 'at_risk_users', 'at_risk_percent', 'sample_margin_of_error',
                   'passed_sample_required']
        projection_cols = ['high_assured', 'low_assured', 'neutral', 'low_at_risk', 'high_at_risk',
                   'market_cap', 'high_assured', 'low_assured', 'low_at_risk', 'high_at_risk']
        csv_writer.writerow([''])
        csv_writer.writerow([''] * 6 + ['confidence', 'z-score'])
        csv_writer.writerow([''] * 6 + ['90%', str(1.645)])
        csv_writer.writerow([''] * 6 + ['95%', str(1.96)])
        csv_writer.writerow([''] * 6 + ['99%', str(2.576)])
        csv_writer.writerow([''])
        csv_writer.writerow(['inputs'] + [''] * 8 + ['sample'] + [''] * 9 + ['projected customer distribution']
                            + [''] * 4 + ['projected opportunity'])
        csv_writer.writerow(['client', 'topic', 'id', ] + input_cols + sample_cols + projection_cols)
        row_number = 8  # where the column headers are
        for node in client_nodes:
            if node['id']:
                print node['client'], node['topic'], node['id']
                authorization = auths[node['client'].lower()]
                i = 0
                total = 1
                r_count = 0
                row_number += 1
                users = {}
                while True:
                    i += 1
                    print node['client'], node['topic'], node['id'], i
                    headers = {'content-type': 'application/json', 'Authorization': authorization}
                    url_request = 'http://dataengine.predictivescience' + \
                    '.com/dp/v1/nodes/' + str(node['id']) + \
                    '/socialstream?'+params+'&page=' + str(i)

                    r = requests.get(url_request, headers=headers)
                    #write to csv
                    if r.status_code == 200:
                        for result in r.json().get('_items',{}):
                            r_count += 1

                            if users.get(result['username']) is None:
                                users[result['username']] = result
                            elif users[result['username']]['datetime'] < result['datetime']:
                                users[result['username']] = result

                        total = r.json().get('_meta',{}).get('total') if r.json() else None
                        if not total or r_count >= total or i >= total:
                            break
                    else:
                        print "error:", r.status_code, r.url, r.text
                        break
                user_counts = {'assured': 0, 'at_risk': 0, 'neutral': 0}
                for user, content in users.items():
                    if content['sentiment'] > 1:
                        user_counts['assured'] += 1
                    elif content['sentiment'] < -1:
                        user_counts['at_risk'] += 1
                    else:
                        user_counts['neutral'] += 1
                row = [node['client'], node['topic'], node['id'], node['arpu'], node['population'],
                       node['confidence_level'], node['margin_of_error'], node['discount_rate'],
                       node['standard_deviation']]

                # sample_required
                row.append('=((VLOOKUP(F' + str(row_number) + ',$G$3:$H$5,2)^2)*(0.5*(0.5)))/(G' + str(row_number) +
                           '^2)')
                # total_users
                total_users = sum([x for x in user_counts.values()])
                row.append(unicode(total_users))
                # assured_users
                row.append(unicode(user_counts['assured']))
                # assured percent
                row.append(unicode(user_counts['assured']/total_users))
                # neutral_users
                row.append(unicode(user_counts['neutral']))
                # neutral percent
                row.append(unicode(user_counts['neutral'] / total_users))
                # at_risk_users
                row.append(unicode(user_counts['at_risk']))
                # at_risk percent
                row.append(unicode(user_counts['at_risk'] / total_users))
                # sample_margin
                row.append('=VLOOKUP(F' + str(row_number) + ',$G$3:$H$5,2)*(0.5/SQRT(K' + str(row_number) + '))')
                # passed_sample_required
                row.append('=IF(K' + str(row_number) + '>=J' + str(row_number) + ',TRUE)')
                # high_assured
                row.append('=($E$' + str(row_number) + '*(M' + str(row_number) + '+$R$' + str(row_number) + '))')
                # low_assured
                row.append('=($E$' + str(row_number) + '*(M' + str(row_number) + '-$R$' + str(row_number) + '))')
                # neutral
                row.append('=($E$' + str(row_number) + '*(O' + str(row_number) + '))')
                # low_at_risk
                row.append('=($E$' + str(row_number) + '*(Q' + str(row_number) + '-$R$' + str(row_number) + '))')
                # high_at_risk
                row.append('=($E$' + str(row_number) + '*(Q' + str(row_number) + '+$R$' + str(row_number) + '))')
                # market_cap
                row.append('=E' + str(row_number) + '*D' + str(row_number) + '')
                # high_assured
                row.append('=(($T$' + str(row_number) + ')*$D$' + str(row_number) + ')-(($T$' + str(row_number) +
                           '*$D$' + str(row_number) + ')*$H$' + str(row_number) + ')')
                # low_assured
                row.append('=(($U$' + str(row_number) + ')*$D$' + str(row_number) + ')-(($U$' + str(row_number) +
                           '*$D$' + str(row_number) + ')*$H$' + str(row_number) + ')')
                # low_at_risk
                row.append('=((($W$' + str(row_number) + ')*$D$' + str(row_number) + ')-(($W$' + str(row_number) +
                           '*$D$' + str(row_number) + ')*$H$' + str(row_number) + '))*-1')
                # high_at_risk
                row.append('=((($X$' + str(row_number) + ')*$D$' + str(row_number) + ')-(($X$' + str(row_number) +
                           '*$D$' + str(row_number) + ')*$H$' + str(row_number) + '))*-1')

                csv_writer.writerow(row)

def get_value(data, key):
    for k in key.split("."):
        data = data[k]
    return data


if __name__ == '__main__':
    # with open('test_creditcard.csv', 'rU') as f:
    # client_nodes = csv.DictReader(f)
# client_nodes = [{'client': 'Verizon', 'topic': 'Agent', 'id': '0b81aa2e-b27f-441e-bd3e-634acd8fc313'},
                # {'client': 'Verizon', 'topic': 'System', 'id': '1b0b5d6c-743d-4a17-abf5-a2317e7478b2'}
# ]
# client_nodes = [{'client': 'NFL', 'topic': 'NFL Shop', 'id': 'f7b66ec9-3be0-429b-810a-230c7bf1c1cc'},
#                 {'client': 'NFL', 'topic': 'NFL Ticket Exchange',
#                  'id': '8ba58170-174e-472e-bec5-c8e78cd9871d'},
#                 {'client': 'NFL', 'topic': 'NFL Network Brand',
#                  'id': '779a27d0-891e-4b5f-a60a-95d214df3c27'}
# ]
# client_nodes = [
#     {'client': 'Airlines', 'topic': 'Advertising', 'id': '9e5ebe72-6ba7-4a9f-8afc-584e5e07a90c'},
#     {'client': 'Airlines', 'topic': 'Aircraft', 'id': '338f69aa-ffbf-4ba2-aa0a-9d43bdffca5d'},
#     {'client': 'Airlines', 'topic': 'Airport Facilities', 'id': '16a0dba4-9d10-4a23-835d-73d5fe108bc2'},
#     {'client': 'Airlines', 'topic': 'Baggage', 'id': '2c448d6f-d2c4-4f12-a7a3-8903a74c556b'},
#     {'client': 'Airlines', 'topic': 'Bodily Injury', 'id': 'dca28bfa-f94b-4f40-a125-bdef2732bf1c'},
#     {'client': 'Airlines', 'topic': 'Brand', 'id': '4558dc55-c0bb-45bb-a182-06cf75d9f67f'},
#     {'client': 'Airlines', 'topic': 'Credit Card', 'id': '31778f3c-23ac-4f7f-84c2-5996a1c26b57'},
#     {'client': 'Airlines', 'topic': 'Customer Service', 'id': '6685c1eb-c974-4cd5-acaa-626b30dea47d'},
#     {'client': 'Airlines', 'topic': 'Disability', 'id': '9d890e62-3727-439c-8f4c-cbc14aa77ead'},
#     {'client': 'Airlines', 'topic': 'Diversity', 'id': '7c7cf121-acd9-4e81-bd47-ca22106876d8'},
#     {'client': 'Airlines', 'topic': 'Fares', 'id': '9777b655-cf43-476e-aee0-94d18ea094ed'},
#     {'client': 'Airlines', 'topic': 'Flights', 'id': '07484ce6-acdc-4177-9431-3cf8b61098a1'},
#     {'client': 'Airlines', 'topic': 'Loyalty Programs', 'id': 'f525ef66-9308-445e-aefc-7a3743b2ce66'},
#     {'client': 'Airlines', 'topic': 'Oversales', 'id': '587f099e-8405-43ec-b74c-76178d28c47a'},
#     {'client': 'Airlines', 'topic': 'Refunds/Change Fees/Adjustments', 'id': 'ebe77134-3759-4deb-9ade-70204c0820c2'},
#     {'client': 'Airlines', 'topic': 'Reservations/Ticketing/Boarding',
#      'id': '660bd3e9-7a5f-4c5d-8209-87897615b295'},
#     {'client': 'Airlines', 'topic': 'Security', 'id': '164c611f-520c-4cc9-b4f3-3d7e1c125e47'}
# ]
    def str2bool(v):
        """ Converts common boolean strings into their True/False equivalents

        :param v: The string value
        :return: The Boolean equivalent of :param v:
        :rtype: Boolean
        """
        return False if not v else v.lower() in ("yep", "yes", "true", "t", "1", "y")


    parser = argparse.ArgumentParser(description='Predictive Science Digital Pulse - Get Value')
    parser.register('type', 'bool', str2bool)

    parser.add_argument("-s", "--sourcefile", dest="source_file", required=True,
                        help="The file with clients, topics, and ids")
    parser.add_argument("-t", "--destination", dest="tgt_file", required=False,
                        help="The /file/path/to/the/results.csv. Default is a file called results.csv and is in "
                             "the directory the script is run from.",
                        default="results.csv")
    parser.add_argument("-p", "--params", dest="params", required=True,
                        help="The endpoint parameters example: "
                        "\"start-date=2015-02-22T00:00:00-07:00&end-date=2015-04-21T23:59:59-06:00' + \
                        '&sentiment=all&post-source=all\" \nDo not set page as parameter. \nFor more information on"
                        " parameter configurations see the List of Parameters and Filters on "
                        "https://predictivescience.atlassian.net/wiki/display/CX/API+Overview")
    parser.add_argument("-v", "--clv", dest="clv", required=False,
                        help="The Customer Lifetime Value of each user in the result. Defaults to 500.",
                        default=500, type=int)
    parser.add_argument("-a", "--acceptrate", dest="accept_rate", required=False,
                        help="The percentage of customers that will change their purchase habits after agreeing"
                             "with a post. Must be between 0 and 1.",
                        default=0.01, type=float)

    args = parser.parse_args()
    globals().update(vars(args))

    with open(source_file, 'rU') as f:
        client_nodes = csv.DictReader(f)
        main_agg(client_nodes=client_nodes, tgt_file=tgt_file, params=params, clv=clv, accept_rate=accept_rate)
