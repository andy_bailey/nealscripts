from flask import Flask, request
from flask import Response
from flask import stream_with_context
import os
import requests
from multiprocessing import Process, Pipe
import content_counting
import tempfile

app = Flask(__name__)


@app.route('/', defaults={'path': ''}, methods=['GET', 'POST'])
@app.route('/<path:path>', methods=['GET', 'POST'])
def stream_post(*args, **kwargs):

    # using multiprocessing, we can pass one end of a named pipe
    # to the worker, but unfortunately the script we are running
    # would have to expect a socket instead of a file descriptor
    def generate(fh):
        content_counting.main(fh)
        fh.seek(0)
        for line in fh:
            yield line

    tmpf = tempfile.TemporaryFile()
    return Response(generate(tmpf), mimetype='text/csv')

if __name__ == '__main__':
    app.debug = True
    app.run()
