from elasticsearch import Elasticsearch
import csv, codecs, cStringIO
from elasticsearch import helpers
import json

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

def output_to_csv():
    query = """{"query":{"match_all":{}}}"""


    es = Elasticsearch(["54.86.46.169:9200"])

    results = helpers.scan(client=es, index='concussion_all', doc_type='interactions', query=query)

    with open('concussion_all.csv', 'wb') as csvfile:
        csvwriter = UnicodeWriter(csvfile)
        csvwriter.writerow(['_id', 'datetime', 'author', 'description', 'link', 'title', 'link_description','site_name',
                            'content'])
        for result in results:
            if result['_source']['interaction'].get('links', {}) \
            and result['_source']['interaction'].get('links', {}).get('meta', {}).get('opengraph',[{}])[0] is not None:
                site_name = result['_source']['interaction'].get('links', {}).get('meta', {}).get('opengraph',
                                                                                                  [{}])[0].get(
                    'site_name', '')
            else:
                site_name = ''
            csvwriter.writerow([result["_id"],
                                    result['_source']['interaction']['interaction']['created_at'],
                                    result['_source']['interaction']['twitter'].get('user',{}).get('screen_name',''),
                                    result['_source']['interaction']['twitter'].get('user',{}).get('description',''),
                                    result['_source']['interaction'].get('links',{}).get('url',[''])[0],
                                    result['_source']['interaction'].get('links',{}).get('title',[''])[0],
                                    result['_source']['interaction'].get('links', {}).get('meta',{}).get('description', [''])[0],
                                    site_name,
                                    result['_source']['interaction']['interaction']['content']
                ])

def get_topics():
    topics = {"G": "Medical Clearance", "P": "Concussion Protocol", "H": "Players Health", "L": "Lawsuit",
              "M1": "Myth 1: Concussions Increasing", "F1": "Fact 1: Concussions Decreasing",
              "M2": "Myth 2: NFL expects 33%", "F2": "Fact 2: Actuary overprojects 33%",
              "X": "Unrelated"}
    return topics

def translate_topics(k):
    topics = {"G":"Medical Clearance", "P": "Concussion Protocol", "H": "Players Health",  "L": "Lawsuit",
              "M1": "Myth 1: Concussions Increasing", "F1": "Fact 1: Concussions Decreasing",
              "M2": "Myth 2: NFL expects 33%", "F2": "Fact 2: Actuary overprojects 33%",
              "X": "Unrelated"}
    return topics[k]

def build_global_query(entities, query={}, label='', interval=''):
    # build in aggs
    query.update(build_agg(entities=entities, interval=interval))
    print label+"|"+json.dumps(query)
    return query

def build_agg(entities, interval=''):
    if not interval:
        temp = {"aggs": {}}
        query = temp
    else:
        temp = {"aggs": {
                    "vol_over_time": {
                        "date_histogram": {
                            "field": "interaction.interaction.created_at",
                            "interval": "week"
                            },
                            "aggs": {}
                        }
                    }
                }
        query = temp["aggs"]["vol_over_time"]
    for entity, entity_query in entities.items():
        # @mentions - original
        query['aggs'].update(build_at_mentions(entity, entity_query, retweet=False))

        # @mentions - retweet
        query['aggs'].update(build_at_mentions(entity, entity_query, retweet=True))

        # originated - original
        query['aggs'].update(build_original(entity=entity, entity_query=entity_query, retweet=False))

        # originated - retweet
        query['aggs'].update(build_original(entity=entity, entity_query=entity_query, retweet=True))

        # content_mentions_original
        query['aggs'].update(build_content_mentions(entity=entity, content_desc="_content", entity_query=entity_query,
                                                    fields=["interaction.interaction.content"],
                                                    retweet=False))
        # content_mentions_retweet
        query['aggs'].update(build_content_mentions(entity=entity, content_desc="_content", entity_query=entity_query,
                                                    fields=["interaction.interaction.content"],
                                                    retweet=True))

        # title_mentions_original
        query['aggs'].update(build_content_mentions(entity=entity, content_desc="_title", entity_query=entity_query,
                                                    fields=["interaction.links.title"],
                                                    retweet=False))
        # title_mentions_retweet
        query['aggs'].update(build_content_mentions(entity=entity, content_desc="_title", entity_query=entity_query,
                                                    fields=["interaction.links.title"],
                                                    retweet=True))

        # link_desc_mentions_original
        query['aggs'].update(build_content_mentions(entity=entity, content_desc="_link_desc", entity_query=entity_query,
                                                    fields=["interaction.links.meta.description",
                                                            "interaction.links.meta.opengraph.description"],
                                                    retweet=False))
        # link_desc_mentions_retweet
        query['aggs'].update(build_content_mentions(entity=entity, content_desc="_link_desc", entity_query=entity_query,
                                                    fields=["interaction.links.meta.description",
                                                            "interaction.links.meta.opengraph.description"],
                                                    retweet=True))

        # verified_original
        query['aggs'].update(build_verified(entity=entity, entity_query=entity_query, retweet=False, verified=True))

        # verified_retweet
        query['aggs'].update(build_verified(entity=entity, entity_query=entity_query, retweet=True, verified=True))

        # non-verified_original
        query['aggs'].update(build_verified(entity=entity, entity_query=entity_query, retweet=False, verified=False))

        # non-verified_retweet
        query['aggs'].update(build_verified(entity=entity, entity_query=entity_query, retweet=True, verified=False))

    return temp

def build_at_mentions(entity, entity_query, retweet=False):
    if retweet:
        exists_or_missing = 'exists'
        content_type = 'retweet'
    else:
        exists_or_missing = 'missing'
        content_type = 'original'
    temp = {entity+"_@mentions_"+content_type: {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "query": {
                                    "query_string": {
                                        "fields": ["interaction.interaction.mentions"],
                                        "query": entity_query
                                    }
                                }
                            },

                            {
                                exists_or_missing: {
                                    "field": "interaction.twitter.retweet.count"
                                }
                            }

                        ]
                    }
                }
            }
        }
    return temp

def build_original(entity, entity_query, retweet=False):
    if retweet:
        exists_or_missing = 'exists'
        fields = ["interaction.twitter.retweeted.user.screen_name"]
        content_type = 'retweet'
    else:
        exists_or_missing = 'missing'
        fields = ["interaction.twitter.user.screen_name"]
        content_type = 'original'
    temp = {
        entity + "_" + content_type: {
            "filter": {
                "bool": {
                    "must": [
                        {
                            "query": {
                                "query_string": {
                                    "fields": fields,
                                    "query": entity_query
                                }
                            }
                        },
                        {
                            exists_or_missing: {
                                "field": "interaction.twitter.retweet.count"
                            }
                        }
                    ]
                }
            }
        }
    }
    return temp

def build_content_mentions(entity, entity_query, content_desc, fields, retweet=False):
    if retweet:
        exists_or_missing = 'exists'
        content_type = 'retweet'
    else:
        exists_or_missing = 'missing'
        content_type = 'original'
    if not entity_query:
        entity_query = entity

    temp = {
        entity + content_desc +"_mentions_" + content_type: {
            "filter": {
                "bool": {
                    "must": [
                        {
                            "query": {
                                "query_string": {
                                    "fields": fields,
                                    "query": entity_query
                                }
                            }
                        },
                        {
                            exists_or_missing: {
                                "field": "interaction.twitter.retweet.count"
                            }
                        }
                    ]
                }
            }
        }
    }
    return temp

def build_verified(entity, entity_query, retweet=False, verified=True):
    if retweet:
        fields = ["interaction.twitter.user.description"]
        term_fields = "interaction.twitter.user.verified"
        content_type = 'retweet'
    else:
        fields = ["interaction.twitter.retweeted.user.description"]
        term_fields = "interaction.twitter.retweeted.user.verified"
        content_type = 'original'
    if not verified:
        verified_prefix = 'non-'
    else:
        verified_prefix = ''

    temp = {
        entity+"_"+verified_prefix+"verified_"+content_type: {
            "filter": {
                "bool": {
                    "must": [
                        {
                            "query": {
                                "query_string": {
                                    "fields": fields,
                                    "query": entity_query
                                }
                            }
                        },
                        {
                            "term": {
                                term_fields: verified
                            }
                        }
                    ]
                }
            }
        }
    }
    return temp

def get_global_query_template():
    query = {
              "query": {
                "filtered": {
                  "filter": {
                    "bool": {
                      "must": [
                        {
                          "bool": {
                            "should": [
                            ]
                          }
                        }
                      ]
                    }
                  }
                }
              }
            }

    return query

def build_link_query(query, link):
    link_query = {
        "term": {
            "interaction.links.meta.opengraph.url": link
        }
    }

    query['query']['filtered']['filter']['bool']['must'][0]['bool']['should'].append(link_query)
    return query

def build_content_query(query, query_string, fields):
    temp = {
              "query": {
                "query_string": {
                  "fields": fields,
                  "query": query_string
                }
              }
            }
    query['query']['filtered']['filter']['bool']['must'][0]['bool']['should'].append(temp)
    return query

def main_counts(es, links, topics_queries, labels, csvwriter):
    #write header
    csvwriter.writerow(['key', 'label', 'description', 'entity', 'content_description', 'content_type', 'docs'])
    # build links query for each label type
    for label, description in labels.items():
        global_query = get_global_query_template()
        # build the link query
        for link in links:
            #find links asscociated with label
            for link_label in link['label'].split(','):
                # add link to query
                if link_label.strip().upper() == label:
                    global_query = build_link_query(query=global_query, link=link['link'])

        # build topic query
        for topic_query in topic_queries:
            if topic_query['topic_label'] == label and topic_query['query']:
                global_query = build_content_query(query=global_query, query_string=topic_query['query'],
                                                   fields=[x for x in topic_query['fields'].split(",")])

        entities = {"espn": "*espn*", "nbc": "*nbc*", "cbs": "*cbs*", "fox": "fox*"}
        global_query = build_global_query(entities=entities, query=global_query, label=label)

        results = es.search(index='concussion_all', doc_type='interactions', body=json.dumps(global_query),
                            search_type='count')

        for k, v in results['aggregations'].items():
            if '_'.join(k.split("_")[1:-1]) != '':
                content_description = '_'.join(k.split("_")[1:-1])
            else:
                content_description = k.split("_")[-1]

            csvwriter.writerow([k,
                                label,
                                description,
                                k.split("_")[0],
                                content_description,
                                k.split("_")[-1],
                                unicode(v['doc_count'])])

def main_time_counts(es, links, topics_queries, labels, interval, csvwriter):
    #write header
    csvwriter.writerow(['datetime','key', 'label', 'description', 'entity', 'content_description',
                        'content_type', 'docs'])
    # build links query for each label type
    for label, description in labels.items():
        global_query = get_global_query_template()
        # build the link query
        for link in links:
            #find links asscociated with label
            for link_label in link['label'].split(','):
                # add link to query
                if link_label.strip().upper() == label:
                    global_query = build_link_query(query=global_query, link=link['link'])

        # build topic query
        for topic_query in topic_queries:
            if topic_query['topic_label'] == label and topic_query['query']:
                global_query = build_content_query(query=global_query, query_string=topic_query['query'],
                                                   fields=[x for x in topic_query['fields'].split(",")])

        entities = {"espn": "*espn*", "nbc": "*nbc*", "cbs": "*cbs*", "fox": "fox*"}
        global_query = build_global_query(entities=entities, query=global_query, label=label, interval=interval)

        results = es.search(index='concussion_all', doc_type='interactions', body=json.dumps(global_query),
                            search_type='count')

        for bucket in results['aggregations']['vol_over_time']['buckets']:
            bucket.pop('doc_count')
            bucket.pop('key')
            datetime = bucket.pop('key_as_string')
            for k, v in bucket.items():
                if '_'.join(k.split("_")[1:-1]) != '':
                    content_description = '_'.join(k.split("_")[1:-1])
                else:
                    content_description = k.split("_")[-1]

                csvwriter.writerow([datetime, k,
                                    label,
                                    description,
                                    k.split("_")[0],
                                    content_description,
                                    k.split("_")[-1],
                                    unicode(v['doc_count'])])

def main_time_counts(es, links, topics_queries, labels, interval, csvwriter):
    #write header
    csvwriter.writerow(['datetime','key', 'label', 'description', 'entity', 'content_description',
                        'content_type', 'docs'])
    # build links query for each label type
    for label, description in labels.items():
        global_query = get_global_query_template()
        # build the link query
        for link in links:
            #find links asscociated with label
            for link_label in link['label'].split(','):
                # add link to query
                if link_label.strip().upper() == label:
                    global_query = build_link_query(query=global_query, link=link['link'])

        # build topic query
        for topic_query in topic_queries:
            if topic_query['topic_label'] == label and topic_query['query']:
                global_query = build_content_query(query=global_query, query_string=topic_query['query'],
                                                   fields=[x for x in topic_query['fields'].split(",")])

        entities = {"espn": "*espn*", "nbc": "*nbc*", "cbs": "*cbs*", "fox": "fox*"}
        global_query = build_global_query(entities=entities, query=global_query, label=label, interval=interval)

        results = es.search(index='concussion_all', doc_type='interactions', body=json.dumps(global_query),
                            search_type='count')

        for bucket in results['aggregations']['vol_over_time']['buckets']:
            bucket.pop('doc_count')
            bucket.pop('key')
            datetime = bucket.pop('key_as_string')
            for k, v in bucket.items():
                if '_'.join(k.split("_")[1:-1]) != '':
                    content_description = '_'.join(k.split("_")[1:-1])
                else:
                    content_description = k.split("_")[-1]

                csvwriter.writerow([datetime, k,
                                    label,
                                    description,
                                    k.split("_")[0],
                                    content_description,
                                    k.split("_")[-1],
                                    unicode(v['doc_count'])])

def top_links_over_time(es, csvwriter):

    query = {
        "aggs": {
            "histogram": {
                "date_histogram": {
                    "field": "interaction.interaction.created_at",
                    "interval": "day"
                },
                "aggs": {
                    "top_links": {
                        "terms": {
                            "field": "interaction.links.meta.opengraph.url",
                            "size": 1
                        }
                    }
                }
            }
        }
    }

    #write header
    csvwriter.writerow(['datetime', 'link', 'docs'])

    results = es.search(index="concussion_all", doc_type="interactions", body=json.dumps(query), search_type='count')

    for bucket in results['aggregations']['histogram']['buckets']:
        bucket.pop('doc_count')
        bucket.pop('key')
        datetime = bucket.pop('key_as_string')
        for b in bucket['top_links']['buckets']:
            csvwriter.writerow([datetime, b['key'], unicode(b['doc_count'])])


def main(outfile):

    es = Elasticsearch(["54.86.46.169:9200"])
    with open('link_types.csv', 'rU') as f:
        links = [x for x in csv.DictReader(f)]

    with open('topic_queries.csv', 'rU') as f:
        topic_queries = [x for x in csv.DictReader(f)]

#    with open('concussion_top_link.csv', 'wb') as outfile:
    csvwriter = UnicodeWriter(outfile)

        # get each label type
    labels = get_topics()
    top_links_over_time(es, csvwriter)
        # main_time_counts(es=es, links=links, topics_queries=topic_queries, labels=labels, interval='week',
        #                  csvwriter=csvwriter)

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("output", argparse.FileType('wb'), default=sys.stdout)
    main()
