# -- coding: utf-8 --
import psstring as ps
import csv, codecs, cStringIO

header = ['client', 'topic', 'id', 'datetime', 'username', 'sentiment', 'userUrl', 'sourceInfo.retweets', 'sourceInfo'
          '.likeCount', 'source', 'avatar', 'images', 'messageUrl', '_id',	'content', 'clean_content']

with open("verizon_content.csv", "rU") as r:
    content_csv = [x for x in csv.DictReader(r)]

with open('cleaned_verizon.csv', 'w') as w:
    csv_writer = csv.writer(w)
    csv_writer.writerow(header)
    for line in content_csv:
        if line['content']:
            line['clean_content'] = ps.clean_content(line['content'])
            row = []
            for c in header:
                row.append(line[c])
            csv_writer.writerow(row)