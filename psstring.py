# -*- coding: utf-8 -*-
"""
Predictive Science function library for string cleaning and tokenizing
"""
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.stem.wordnet import WordNetLemmatizer


cachedStopWords = stopwords.words("english")
special_chars = ['!', '.', ',', '^', '&', '*', '""', "''", "''", '``', ',',
                 '``']
stemmer = SnowballStemmer("english")
wnl = WordNetLemmatizer()


def remove_html(content):
    """
    Remove html from a string

    :param content: string
    :rtype string: content without html
    """
    return nltk.clean_html(content)


def remove_urls(content):
    """
    Remove urls from a string

    :param content: string
    :rtype string: content without urls
    """
    content = re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]
                         {2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s
                         ()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s
                         ()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''',
                     '', content).strip()
    return content


def remove_handles(content):
    """
    Remove twitter handles from a string

    :param content: string
    :rtype string: content without twitter handles
    """
    content = re.sub(r'@[a-zA-Z0-9_\-\.]*', '', content).strip()
    return content


def split_sentences(content):
    """
    Split a string into sentences

    :param content: string
    :rtype string: content split into sentences
    """
    content = re.sub(r'([a-zA-Z])\.([a-zA-Z])', '\g<1>. \g<2>', content)
    return content


def token_stop_stem(content):
    """
    standardizes text to lower case, removes stop words, special characters,
    and tokenizes the content string.
    :param content:string
    :rtype list: list of tokens in content
    """
    tokenized = [stemmer.stem(x.lower()) for x in nltk.word_tokenize(content)
                 if not (not (x.lower() not in cachedStopWords) or not (len(x) > 1) or not (x not in special_chars))]
    return tokenized


def find_keywords(content):
    """
    returns token with len > 2 in a content string

    :param content:string
    :rtype list: list of tokens len > 2
    """
    clean_post = split_sentences(remove_handles(remove_urls(remove_html(content))))
    tokenized = []
    for x in nltk.word_tokenize(clean_post):
        if x[-1] == '.':
            x = x.replace('.', '')
        if x.lower() not in cachedStopWords \
                and len(x) > 2 \
                and x not in special_chars:
            if x not in ["amp", "'ll", "n't", "...", "'ve"]:
                if 'u/' not in x \
                        and "'" not in x:
                    tokenized.append(wnl.lemmatize(x.lower()))

    return tokenized

def clean_content(content):

    clean_post = split_sentences(remove_handles(remove_urls(remove_html(content))))

    return clean_post


def remove_stopwords(content):
    pass


def remove_special_chars(content):
    pass


def create_bigrams(tokens):
    return nltk.bigrams(tokens)


def create_trigrams(tokens):
    return nltk.trigrams(tokens)


def test_psstring():
    from nose.tools import assert_equal

    content = "<b>http://go.com www.here.com @art-z_one9.far_out You are WILDLY Crazy.You Hobo.</b>"
    removed_html = remove_html(content)
    removed_handles = remove_handles(removed_html)
    removed_urls = remove_urls(removed_handles)
    spl_sent = split_sentences(removed_urls)
    tss = token_stop_stem(spl_sent)
    keywords = find_keywords(content)

    assert_equal(removed_html, "http://go.com www.here.com @art-z_one9.far_out You are WILDLY Crazy.You Hobo.")
    assert_equal(removed_handles, "http://go.com www.here.com  You are WILDLY Crazy.You Hobo.")
    assert_equal(removed_urls, "You are WILDLY Crazy.You Hobo.")
    assert_equal(spl_sent, "You are WILDLY Crazy. You Hobo.")
    assert_equal(str(tss), "[u'wild', u'crazy.', u'hobo']")
    assert_equal(str(keywords), "['wildly', 'crazy', 'hobo']")
